#ifndef LIBJSON_LIBJSON_INCLUDE_LIBJSON
#define LIBJSON_LIBJSON_INCLUDE_LIBJSON

#ifndef __cplusplus
  #error "libjson requires C++."
#elsif
  #if __cplusplus < 201703L
    #error "libjson requires C++17 or better."
  #endif
#endif

namespace libjson {

/* C++20: UTF-8 char support is 201811L and UTF-8 char library support is 201907L */
#if __cplusplus < 201907L
  #if defined(_MSC_VER) and defined(_UNICODE)
    #error "Please use Unicode, in what world are you living ? Do you think ALL languages representation are not important ?"
  #endif
  static_assert(sizeof(char) == 1, "Please use Unicode.");
  using string = std::string;
#else
  using string = std::u8string;
#endif


#include <libjson/data/value.h++>



#endif
