#ifndef LIBJSON_LIBJSON
#define LIBJSON_LIBJSON

/* libjson data members. */
#include <libjson/data/array.h++>
#include <libjson/data/boolean.h++>
#include <libjson/data/null.h++>
#include <libjson/data/number.h++>
#include <libjson/data/object.h++>
#include <libjson/data/string.h++>
#include <libjson/data/value.h++>

#endif
