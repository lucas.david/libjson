#ifndef LIBJSON_RESULT
#define LIBJSON_RESULT

#include <variant>

template<typename T, typename Error>
struct result {
public:
  using value_type = T;
  using reference = value_type&;
  using const_reference = value_type const&;

  using error_type = Error;
  using error_reference = error_type&;
  using const_error_reference = error_type const&;

private:
  std::variant<T, Error> internal;

private:
  explicit result(T const& value) : internal(value) {}
  explicit result(T&& value) : internal(value) {}
  explicit result(Error const& error) : internal(error) {}
  explicit result(Error&& error) : internal(error) {}

public:
  static result ok(T const& value) { return result(value); }
  static result ok(T&& value) { return result(value); }
  static result error(Error const& error) { return result(error); }
  static result error(Error&& error) { return result(error); }

public:
  result(result const&) = default;
  result(result&&) noexcept = default;

  reference unwrap() { return std::get<value_type>(internal); }
  const_reference unwrap() const { return std::get<value_type>(internal); }

  error_reference unwrap_error() { return std::get<error_type>(internal); }
  const_error_reference unwrap_error() const { return std::get<error_type>(internal); }

  [[nodiscard]] bool is_ok() const noexcept { return std::holds_alternative<T>(internal); }
  [[nodiscard]] bool is_error() const noexcept { return std::holds_alternative<T>(internal); }

public:
  template<typename Return, typename Function>
  result<Return, Error> map(Function function) {
    return
  }

  //template<typename U>
  //result<U, Error> and(result<U, Error> const& r) { return is_ok() ? r : unwrap_error(); }

  template<typename U, typename Function>
  result<U, Error> and_then(Function&& function) const { return is_ok() ? function(unwrap()) : unwrap_error(); }

  template<typename U, typename Function>
  result<T, U> or_else(Function&& function) { return is_error() ? function(unwrap_error()) : unwrap(); }


};

#endif