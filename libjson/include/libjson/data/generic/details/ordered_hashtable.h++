#ifndef LIBJSON_DATA_GENERIC_DETAILS_ORDERED_HASHTABLE
#define LIBJSON_DATA_GENERIC_DETAILS_ORDERED_HASHTABLE

#include <memory>


namespace json::details {

  struct bucket {

  };

  template<typename Key, typename T>
  struct node {
  private:
    node* next_;

  public:
    K key;
    T value;



  };

  template<typename Key, typename T,
    typename Hash = std::hash<Key>,
    typename KeyEqual = std::equal_to<Key>,
    typename Allocator = std::allocator<T>>
  struct ordered_hashtable {
  public:
    using key_type = Key;
    using value_type = T;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using hasher = Hash;

    using allocator_type = Allocator;
    using key_equal = Equality;
    using pointer = std::allocator_traits<Allocator>::pointer
    using const_pointer = std::allocator_traits<Allocator>::const_pointer
    using reference = value_type&;
    using const_reference = value_type const&;

  private:
    bucket_type* buckets_   = &single_bucket_;
    size_type bucket_count_ = 1;
    node before_begin_;
    size_type element_count = 0;
    rehash_policy rehash_policy_;

    bucket_type single_bucket_;

  public:
    ordered_hashtable(): ordered_hashtable(static_size<size_type>(0)) {}
    explicit ordered_hashtable(size_type bucket_count, Hash const& hash = Hash(), KeyEqual const& equal = KeyEqual(), Allocator& allocator = Allocator());
    ordered_hashtable(size_type bucket_count, Allocator const& allocator) : ordered_hashtable(bucket_count, Hash(), KeyEqual(), allocator) {}
    ordered_hashtable(size_type bucket_count, Hash const& hash, Allocator const& allocator) : ordered_hashtable(bucket_count, hash, KeyEqual(), allocator) {}
    explicit ordered_hashtable(Allocator const& allocator);


  public:



  };


}


#endif