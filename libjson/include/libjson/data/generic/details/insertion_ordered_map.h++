#ifndef LIBJSON_DATA_GENERIC_INTERNAL_ORDERED_MAP
#define LIBJSON_DATA_GENERIC_INTERNAL_ORDERED_MAP

#include <libjson/data/generic/entry.h++>

#include <initializer_list>
#include <vector>
#include <unordered_map>

namespace libjson::json::details {

  template<typename Key, typename T,
           typename KeyEqual = std::equal_to<Key>,
           typename Allocator = std::allocator<entry<Key, T>>>
  struct insertion_ordered_map {
  public:
    using key_type = Key;
    using mapped_type = T;
    using value_type = entry<Key, T>;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using key_equal = KeyEqual;
    using allocator_type = Allocator;
    using reference = value_type&;
    using const_reference = value_type const&;
    using pointer = value_type*;
    using const_pointer = value_type const*;
    using iterator = pointer;
    using const_iterator = const_pointer;

  private:
    std::vector<value_type> internal;

  public:
    constexpr insertion_ordered_map() noexcept(noexcept(Allocator())) = default;
    constexpr insertion_ordered_map(insertion_ordered_map const&) = default;
    constexpr insertion_ordered_map(insertion_ordered_map&&) noexcept = default;
    constexpr insertion_ordered_map(std::initializer_list<entry<Key, T>> initializer_list) : internal(initializer_list) {}

    constexpr insertion_ordered_map& operator=(insertion_ordered_map const&) = default;
    constexpr insertion_ordered_map& operator=(insertion_ordered_map&&)
      noexcept(std::allocator_traits<Allocator>::propagate_on_container_move_assignment::value
        or std::allocator_traits<Allocator>::is_always_equal::value) = default;
    constexpr insertion_ordered_map& operator=(std::initializer_list<entry<Key, T>> initializer_list) { internal = initializer_list; return *this; }

    [[nodiscard]] allocator_type get_allocator() const noexcept { return allocator_type(); }

  public:
    [[nodiscard]] constexpr iterator begin() noexcept { return iterator(internal.begin().operator->()); }
    [[nodiscard]] constexpr const_iterator begin() const noexcept { return const_iterator(internal.begin().operator->()); }

    [[nodiscard]] constexpr iterator end() noexcept { return iterator(internal.end().operator->()); }
    [[nodiscard]] constexpr const_iterator end() const noexcept { return const_iterator(internal.end().operator->()); }

  public:
    [[nodiscard]] constexpr bool empty() const noexcept { return internal.empty(); }

    [[nodiscard]] constexpr size_type size() const noexcept { return internal.size(); }

    [[nodiscard]] constexpr size_type max_size() const noexcept { return internal.size(); }

  public:
    constexpr void clear() noexcept { internal.clear(); }

    template<typename... Args>
    constexpr std::pair<iterator, bool> emplace(key_type const& key, Args&&... args) {
      auto const it = find(key);
      if (it != end())
        return std::make_pair<iterator, bool>(it, false);
      internal.emplace_back(key, std::forward<Args>(args)...);
      return std::make_pair<iterator, bool>(end() - 1, true);
    }
    template<typename... Args>
    constexpr std::pair<iterator, bool> emplace(key_type&& key, Args&&... args) {
      auto const it = find(key);
      if (it != end())
        return std::make_pair<iterator, bool>(it, false);
      internal.emplace_back(std::forward<key_type>(key), std::forward<Args>(args)...);
      return std::make_pair<iterator, bool>(end() - 1, true);
    }

    constexpr iterator erase(iterator position) { return internal.erase(position); }
    constexpr iterator erase(const_iterator first, const_iterator last) { return internal.erase(first, last); }
    constexpr size_type erase(key_type const& key) {
      auto it = find(key);
      if (it != end()) {
        internal.erase(it);
        return 1;
      }
      return 0;
    }

    constexpr void swap(insertion_ordered_map& rhs) noexcept(std::is_nothrow_swappable<insertion_ordered_map>::value) { internal.swap(rhs.internal); }

  public:
    constexpr value_type& at(size_type index) { return internal.at(index); }
    [[nodiscard]] constexpr value_type const& at(size_type index) const { return internal.at(index); }

    constexpr mapped_type& at(key_type const& key) {
      auto it = find(key);
      if (it != end())
        return it->value;
      throw std::out_of_range("insertion_ordered_map::at(key_type const&) failed: key do not exist.");
    }
    [[nodiscard]] constexpr mapped_type const& at(key_type const& key) const {
      auto const it = find(key);
      if (it != end())
        return it->value;
      throw std::out_of_range("insertion_ordered_map::at(key_type const&) failed: key do not exist.");
    }

    [[nodiscard]] constexpr value_type& operator[](size_type index) noexcept { return internal[index]; }
    [[nodiscard]] constexpr value_type const& operator[](size_type index) const noexcept { return internal[index]; }

    constexpr mapped_type& operator[](key_type const& key) {
      auto const it = find(key);
      if (it != end())
        return it->value;
      internal.emplace_back(key, mapped_type());
      return internal.back().value;
    }
    constexpr mapped_type& operator[](key_type&& key) {
      auto const it = find(key);
      if (it != end())
        return it->value;
      internal.emplace_back(std::forward<key_type>(key), mapped_type());
      return internal.back().value;
    }

    [[nodiscard]] constexpr iterator find(key_type const& key) {
#if 202002L < __cplusplus
      for (auto it = begin(); it != end(); ++it)
        if (key_equal()(key, (*it).key))
          return it;
      return end();
#else
      return std::find_if(begin(), end(), [&](auto const& entry) { return key_equal()(key, entry.key); });
#endif
    }
    [[nodiscard]] constexpr const_iterator find(key_type const& key) const {
#if 202002L < __cplusplus
      for (auto it = begin(); it != end(); ++it)
        if (key_equal()(key, (*it).key))
          return it;
      return end();
#else
      return std::find_if(begin(), end(), [&](auto const& entry) { return key_equal()(key, entry.key); });
#endif
    }

    [[nodiscard]] constexpr bool contains(key_type const& key) const noexcept {
      return std::any_of(internal.begin(), internal.end(), [&] (auto const& item) { return key_equal()(key, item.key); });
    }

  public:
    constexpr key_equal key_eq() const { return key_equal(); }

  public:
    [[nodiscard]] constexpr bool operator==(insertion_ordered_map const& rhs) const noexcept { return internal == rhs.internal; }
    [[nodiscard]] constexpr bool operator!=(insertion_ordered_map const& rhs) const noexcept { return internal != rhs.internal; }
  };

}

#endif
