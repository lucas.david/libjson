#ifndef LIBJSON_DATA_GENERIC_BASIC_OBJECT
#define LIBJSON_DATA_GENERIC_BASIC_OBJECT

#include <libjson/data/string.h++>
#include <libjson/data/generic/details/insertion_ordered_map.h++>

namespace libjson::json {

  template<template<typename> typename Allocator>
  struct basic_value;

  template<template<typename> typename Allocator = std::allocator>
  struct basic_object final {
  private:
    friend json::basic_value<Allocator>;

  public:
    using key_type = json::string;
    using mapped_type = json::basic_value<Allocator>;
    using value_type = json::entry<json::string, json::basic_value<Allocator>>;
    using size_type = std::size_t;
    using difference_type [[maybe_unused]] = std::ptrdiff_t;
    using key_equal = std::equal_to<json::string>;
    using allocator_type = Allocator<json::entry<json::string, json::basic_value<Allocator>>>;
    using reference [[maybe_unused]] = value_type&;
    using const_reference [[maybe_unused]] = value_type const&;
    using pointer [[maybe_unused]] = value_type*;
    using const_pointer [[maybe_unused]] = value_type const*;
    using iterator = typename details::insertion_ordered_map<key_type, mapped_type, key_equal, allocator_type>::iterator;
    using const_iterator = typename details::insertion_ordered_map<key_type, mapped_type, key_equal, allocator_type>::const_iterator;

  private:
    details::insertion_ordered_map<key_type, mapped_type, key_equal, allocator_type> internal;

  public:
    constexpr basic_object(basic_object const& rhs) : internal(rhs.internal) {}
    constexpr basic_object& operator=(basic_object const&) = default;

  private:
#if __cplusplus < 201907L
    [[nodiscard]] std::string dump(unsigned int const indent, unsigned int depth) const {
      std::string result = "{";
      if (!internal.empty()) {
        unsigned int const whitespaces = indent * depth;
        for (std::size_t index = 0; index != internal.size() - 1; ++index)
          result.append(whitespaces, ' ').operator+=('\"')
            .operator+=(internal[index].key.dump()).operator+=("\": ")
            .operator+=(internal[index].value.dump(indent, depth + 1)).operator+=(",\n");
        result.append(whitespaces, ' ').operator+=('\"')
          .operator+=(internal.back().key.dump()).operator+=("\": ")
          .operator+=(internal.back().value.dump(indent, depth + 1)).operator+=(",\n");
      }
      return result += '}';
    }
#endif

  public:
    constexpr basic_object() noexcept(noexcept(Allocator<json::entry<json::string, json::basic_value<Allocator>>>())) = default;
    constexpr basic_object(basic_object&&) noexcept = default;
    constexpr basic_object(std::initializer_list<json::entry<json::string, json::basic_value<Allocator>>> initializer_list) : internal() {
      for (auto const& entry : initializer_list)
        operator[](entry.key.clone()) = entry.value.clone();
    }

    constexpr basic_object& operator=(basic_object&&) noexcept(std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value
      or std::allocator_traits<allocator_type>::is_always_equal::value) = default;
    constexpr basic_object& operator=(std::initializer_list<json::entry<json::string, json::basic_value<Allocator>>> initializer_list) {
      internal.clear();
      for (auto const& entry : initializer_list)
        operator[](entry.key.clone()) = entry.value.clone();
      return *this;
    }

  public:
    [[nodiscard]] iterator begin() noexcept { return iterator(internal.begin()); }
    [[nodiscard]] const_iterator begin() const noexcept { return const_iterator(internal.begin()); }

    [[nodiscard]] iterator end() noexcept { return iterator(internal.end()); }
    [[nodiscard]] const_iterator end() const noexcept { return const_iterator(internal.end()); }

  public:
    [[nodiscard]] bool empty() const noexcept { return internal.empty(); }
    [[nodiscard]] size_type size() const noexcept { return internal.size(); }

  public:
    [[nodiscard]] mapped_type const& at(key_type&& key) const { return internal.at(std::forward<key_type>(key)); }

    mapped_type& operator[](key_type&& key) { return internal.operator[](std::forward<key_type>(key)); }

    [[nodiscard]] bool contains(key_type const& key) const noexcept { return internal.contains(key); }

  public:
    [[nodiscard]] basic_object clone() const { return *this; }

#if __cplusplus < 201907L
    [[nodiscard]] std::string dump() const {
      std::string result = "{";
      if (!internal.empty()) {
        for (std::size_t index = 0; index != internal.size() - 1; ++index)
          result.operator+=('"').operator+=(internal[index].key.dump()).operator+=("\":").operator+=(internal[index].value.dump()).operator+=(',');
        result.operator+=('"').operator+=(internal[internal.size() - 1].key.dump()).operator+=("\":").operator+=(internal[internal.size() - 1].value.dump()).operator+=(',');
      }
      return result += "}";
    }

    [[nodiscard]] std::string dump(unsigned int const indent) const { return dump(indent, 0); }
#else
    [[nodiscard]] std::u8string dump() const {
      std::u8string = u8"{";
      if (!internal.empty()) {
        for (std::size_t index = 0; index != internal.size() - 1; ++index)
          result.operator+=(u8'"').operator+=(internal[index].key.dump()).operator+=(u8"\":").operator+=(internal[index].value.dump()).operator+=(u8',');
        result.operator+=(u8'"').operator+=(internal[internal.size() - 1].key.dump()).operator+=(u8"\":").operator+=(internal[internal.size() - 1].value.dump()).operator+=(u8',');
      }
      return result += u8"}";
    }

    [[nodiscard]] std::u8string dump(unsigned int const indent) const { return dump(indent, 0); }
#endif

  public:
    [[nodiscard]] bool operator==(basic_object const& rhs) const noexcept { return internal == rhs.internal; }
    [[nodiscard]] bool operator!=(basic_object const& rhs) const noexcept { return internal != rhs.internal; }
  };

}

#include <functional>

template<template<typename> typename Allocator>
struct std::hash<libjson::json::basic_object<Allocator>> {
public:
  [[nodiscard]] std::size_t operator()(libjson::json::basic_object<Allocator> const& object) const noexcept {
    std::size_t result = 0;
    for (auto const& item : object)
      result ^= std::hash<typename libjson::json::basic_object<Allocator>::value_type>()(item);
    return result;
  }
};

#endif