#ifndef LIBJSON_DATA_GENERIC_BASIC_ARRAY
#define LIBJSON_DATA_GENERIC_BASIC_ARRAY

#include <initializer_list>
#include <string>
#include <utility>
#include <vector>

namespace libjson::json {

  template<template<typename> typename Allocator>
  struct basic_value;

  template<template<typename> typename Allocator = std::allocator>
  struct basic_array final {
  private:
    friend json::basic_value<Allocator>;

  public:
    using value_type = basic_value<Allocator>;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using allocator_type = Allocator<value_type>;
    using reference = value_type&;
    using const_reference = value_type const&;
    using pointer = value_type*;
    using const_pointer = value_type const*;
    using iterator = typename std::vector<value_type>::iterator;
    using const_iterator = typename std::vector<value_type>::const_iterator;

  private:
    std::vector<value_type, allocator_type> internal;

  public:
    constexpr basic_array(basic_array const& rhs) : internal(rhs.internal) {}
    constexpr basic_array& operator=(basic_array const&) = default;

  private:
#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    [[nodiscard]] std::string dump(unsigned int const indent, unsigned int const depth) {
      std::string result = "[";
      if (!internal.empty()) {
        unsigned int const whitespaces = indent * depth;
        for (size_type index = 0; index != internal.size() - 1; ++index)
          result.append(whitespaces, ' ').operator+=(internal[index].dump(indent, depth + 1)).operator+=(",\n");
        result.append(whitespaces, ' ').operator+=(internal.back().dump(indent, depth + 1)).operator+=('\n');
      }
      return result += "]";
    }
#else
    [[nodiscard]] std::string dump(unsigned int const indent, unsigned int const depth) {
      std::u8string result = u8"[";
      if (!internal.empty()) {
        unsigned int const whitespaces = indent * depth;
        for (size_type index = 0; index != internal.size() - 1; ++index)
          result.append(whitespaces, u8' ').operator+=(internal[index].dump(indent, depth + 1)).operator+=(u8",\n");
        result.append(whitespaces, u8' ').operator+=(internal.back().dump(indent, depth + 1)).operator+=(u8'\n');
      }
      return result += u8"]";
    }
#endif

  public:
    constexpr basic_array() = default;
    constexpr basic_array(basic_array&&) noexcept = default;
    constexpr basic_array(std::initializer_list<value_type> initializer_list) : internal() {
      for (auto const& value : initializer_list)
        internal.push_back(value.clone());
    }

    constexpr basic_array& operator=(basic_array&&) noexcept(
    std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value
      or std::allocator_traits<allocator_type>::is_always_equal::value) = default;
    constexpr basic_array& operator=(std::initializer_list<value_type> initializer_list) {
      internal.clear();
      for (auto const& value : initializer_list)
        internal.emplace_back(value.clone());
      return *this;
    }

  public:
    [[nodiscard]] constexpr reference at(size_type index) { return internal.at(index); }
    [[nodiscard]] constexpr const_reference at(size_type index) const { return internal.at(index); }

    [[nodiscard]] constexpr reference operator[](size_type index) noexcept { return internal[index]; }
    [[nodiscard]] constexpr const_reference operator[](size_type index) const noexcept { return internal[index]; }

    [[nodiscard]] constexpr reference front() noexcept { return internal.front(); }
    [[nodiscard]] constexpr const_reference front() const noexcept { return internal.front(); }

    [[nodiscard]] constexpr reference back() noexcept { return internal.back(); }
    [[nodiscard]] constexpr const_reference back() const noexcept { return internal.back(); }

  public:
    [[nodiscard]] constexpr iterator begin() noexcept { return internal.begin(); }
    [[nodiscard]] constexpr const_iterator begin() const noexcept { return internal.begin(); }
    [[nodiscard]] constexpr iterator end() noexcept { return internal.end(); }
    [[nodiscard]] constexpr const_iterator end() const noexcept { return internal.end(); }

  public:
    [[nodiscard]] constexpr bool empty() const noexcept { return internal.empty(); }
    [[nodiscard]] constexpr size_type size() const noexcept { return internal.size(); }
    [[nodiscard]] constexpr size_type max_size() const noexcept { return internal.max_size(); }
    constexpr void reserve(size_type new_capacity) const { return internal.reserve(new_capacity); }
    [[nodiscard]] constexpr size_type capacity() const noexcept { return internal.capacity(); }

  public:
    constexpr void clear() noexcept { internal.clear(); }

#if false /* work in progress. */
    iterator insert(size_type index, value_type const& value) { return internal.insert(iterator(&internal[index]), value); }
    iterator insert(size_type index, value_type&& value) { return internal.insert(iterator(&internal[index]), std::move(value)); }

    template<typename Iterable>
    iterator insert(size_type index, Iterable const& iterable) {
      return internal.insert(begin() + index, iterable.begin(), iterable.end());
    }
    template<typename Iterable>
    iterator insert(size_type index, Iterable&& iterable) {
      return internal.insert(begin() + index, iterable.begin(), iterable.end());
    }
#endif

    constexpr void push_back(value_type&& value) { internal.push_back(std::move(value)); }

    template<typename... Args>
    constexpr auto emplace_back(Args&& ... args) { internal.template emplace_back(std::forward<Args&&>(args)...); }

    constexpr void pop_back() { internal.pop_back(); }

    constexpr void swap(basic_array& rhs) noexcept(noexcept(internal.swap(rhs.internal))) { internal.swap(rhs.internal); }

  public:
    [[nodiscard]] constexpr basic_array clone() const { return *this; }

#if __cplusplus < 201907L
    [[nodiscard]] std::string dump() const {
      std::string result = "[";
      if (!internal.empty()) {
        for (std::size_t index = 0; index != internal.size() - 1; ++index)
          result.operator+=(internal[index].dump()).operator+=(',');
        result += internal.back().dump();
      }
      return result += "]";
    }

    [[nodiscard]] std::string dump(unsigned int const indent) const { return dump(indent, 0); }
#else
    [[nodiscard]] std::u8string dump() const {
      std::u8string result = u8"[";
      if (!internal.empty()) {
        for (std::size_t index = 0; index != internal.size() - 1; ++index)
          result.operator+=(internal[index].dump()).operator+=(u8',');
        result += internal.back().dump();
      }
      return result += u8"]";
    }

    [[nodiscard]] std::string dump(unsigned int const indent) const { return dump(indent, 0); }
#endif

  public:
    [[nodiscard]] bool operator==(basic_array const& rhs) const noexcept { return internal == rhs.internal; }
    [[nodiscard]] bool operator!=(basic_array const& rhs) const noexcept { return internal != rhs.internal; }
  };

}



//template<template<typename> typename Allocator>
//constexpr void std::swap(json::basic_array<Allocator>& lhs, json::basic_array<Allocator>& rhs) { lhs.swap(rhs); }

#include <functional>

template<template<typename> typename Allocator>
struct std::hash<libjson::json::basic_array<Allocator>> {
public:
  [[nodiscard]] std::size_t operator()(libjson::json::basic_array<Allocator> const& array) const noexcept {
    std::size_t result = 0;
    for (auto const& item : array)
      result ^= std::hash<typename libjson::json::basic_array<Allocator>::value_type>()(item);
    return result;
  }
};

#endif