#ifndef LIBJSON_DATA_GENERIC_BASIC_VALUE
#define LIBJSON_DATA_GENERIC_BASIC_VALUE

#include <libjson/data/boolean.h++>
#include <libjson/data/null.h++>
#include <libjson/data/number.h++>
#include <libjson/data/string.h++>
#include <libjson/data/generic/basic_array.h++>
#include <libjson/data/generic/basic_object.h++>

#include <initializer_list>
#include <memory>
#include <variant>

namespace libjson::json {

  template<template<typename> typename Allocator = std::allocator>
  struct basic_value final {
  private:
    using internal_type = std::variant<json::null_t, json::basic_array<Allocator>, json::boolean, json::number, basic_object<Allocator>, json::string>;
    friend json::null_t;
    friend json::basic_array<Allocator>;
    friend json::number;
    friend json::basic_object<Allocator>;
    friend json::string;

  public:
    using array_reference = json::basic_array<Allocator>&;
    using array_const_reference = json::basic_array<Allocator> const&;
    using boolean_reference = json::boolean&;
    using boolean_const_reference = json::boolean const&;
    using number_reference = json::number&;
    using number_const_reference = json::number const&;
    using object_reference = json::basic_object<Allocator>&;
    using object_const_reference = json::basic_object<Allocator> const&;
    using string_reference = json::string;
    using string_const_refernce = json::string const&;

  private:
    internal_type internal;

  private:
    [[nodiscard]] std::string dump(unsigned int indent, unsigned int depth) const { return visit<std::string>({
        [&](json::basic_array<Allocator> const& array) { return array.dump(indent, depth); },
        [&](json::basic_object<Allocator> const& object) { return object.dump(indent, depth); },
        [](auto const& value) { return std::string(value.dump()); }
      });
    }

  private:
    constexpr basic_value(basic_value const& rhs) : internal(rhs.internal) {}
    constexpr basic_value& operator=(basic_value const& rhs) = default;

  public:
    constexpr basic_value() noexcept(std::is_nothrow_default_constructible<internal_type>::value) = default;
    constexpr basic_value(basic_value&& other) noexcept(std::is_nothrow_move_constructible<internal_type>::value) : internal(std::exchange(other.internal, json::null)) {}

    /* json::null related constructors. */
    constexpr basic_value(json::null_t) noexcept(std::is_nothrow_constructible<internal_type, json::null_t>::value) : basic_value() {} /* NOLINT: implicit conversion is intended. */

    /* json::basic_array related constructors. */
    constexpr basic_value(std::initializer_list<basic_value> initializer_list) : internal(json::basic_array<Allocator>(initializer_list)) {}
    constexpr basic_value(json::basic_array<Allocator>&& array) noexcept(std::is_nothrow_constructible<internal_type, json::basic_array<Allocator>&&>::value) : internal(std::move(array)) {} /* NOLINT: implicit conversion is intended. */

    /* json::boolean related constructors. */
    constexpr basic_value(bool value) noexcept(std::is_nothrow_constructible<internal_type, bool>::value) : internal(json::boolean(value)) {}    /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(json::boolean value) noexcept(std::is_nothrow_constructible<internal_type, json::boolean>::value) : internal(value) {} /* NOLINT: implicit conversion is intended. */

    /* json::number related constructors. */
#if __cplusplus < 202002L /* C++20 and concepts library features are 202002L and concepts core features 201907L. */
    constexpr basic_value(std::uint8_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {}  /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(std::uint16_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {} /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(std::uint32_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {} /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(std::uint64_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {} /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(std::int8_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {}   /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(std::int16_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {}  /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(std::int32_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {}  /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(std::int64_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {}  /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(float value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {}         /* NOLINT: implicit conversion is intended. */
    constexpr basic_value(double value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) : internal(json::number(value)) {}        /* NOLINT: implicit conversion is intended. */
#else
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    basic_value(T value) noexcept(std::is_nothrow_constructible<internal_type, T>::value) : internal(json::number(value)) {}         /* NOLINT: implicit conversion is intended. */
#endif
    basic_value(json::number value) noexcept(std::is_nothrow_constructible<internal_type, json::number>::value) : internal(value) {} /* NOLINT: implicit conversion is intended. */

    /* json::basic_object related constructors. */
    basic_value(std::initializer_list<json::entry<json::string, basic_value>> initializer_list) : internal(json::basic_object<Allocator>(initializer_list)) {}
    basic_value(json::basic_object<Allocator>&& object) noexcept(std::is_nothrow_constructible<internal_type, json::basic_object<Allocator>&&>::value) : internal(std::move(object)) {} /* NOLINT: implicit conversion is intended. */

    /* json::basic_string related constructors. */
    basic_value(char const* str) : internal(json::string(str)) {}                                                                               /* NOLINT: implicit conversion is intended. */
    basic_value(json::string&& str) noexcept(std::is_nothrow_constructible<internal_type, json::string&&>::value) : internal(std::move(str)) {} /* NOLINT: implicit conversion is intended. */

    /* --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

    constexpr basic_value& operator=(basic_value&& value) noexcept(std::is_nothrow_move_assignable<internal_type>::value) { internal = std::exchange(value.internal, json::null); return *this; }

    /* json::basic_array related assignment operators. */
    constexpr basic_value& operator=(std::initializer_list<basic_value> initializer_list) { internal = json::basic_array<Allocator>(initializer_list); return *this; }
    constexpr basic_value& operator=(json::basic_array<Allocator>&& array) noexcept(std::is_nothrow_assignable<internal_type, json::basic_array<Allocator>&&>::value) { internal = std::move(array); return *this; }

    /* json::boolean related assignment operators. */
    constexpr basic_value& operator=(bool value) noexcept(std::is_nothrow_assignable<internal_type, json::boolean>::value) { internal = json::boolean(value); return *this; }
    constexpr basic_value& operator=(json::boolean value) noexcept(std::is_nothrow_assignable<internal_type, json::boolean>::value) { internal = value; return *this; }

    /* json::number related assignment operators. */
#if __cplusplus < 202002L /* C++20 and concepts library features are 202002L and concepts core features 201907L. */
    constexpr basic_value& operator=(std::uint8_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(std::uint16_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(std::uint32_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(std::uint64_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(std::int8_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(std::int16_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(std::int32_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(std::int64_t value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(float value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
    constexpr basic_value& operator=(double value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
#else
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    basic_value& operator=(T value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = json::number(value); return *this; }
#endif
    constexpr basic_value& operator=(json::number value) noexcept(std::is_nothrow_assignable<internal_type, json::number>::value) { internal = value; return *this; }

    /* json::basic_object related assignment operators. */
    constexpr basic_value& operator=(std::initializer_list<json::entry<json::string, basic_value>> initializer_list) { internal = json::basic_object<Allocator>(initializer_list); return *this; }
    constexpr basic_value& operator=(json::basic_object<Allocator>&& object) noexcept(std::is_nothrow_assignable<internal_type, json::basic_object<Allocator>&&>::value) { internal = std::move(object); return *this; }

    /* json::basic_string related assignment operators. */
#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    basic_value& operator=(char const* str) { internal = json::string(str); return *this; }
#else
    basic_value& operator=(char8_t const* str) { internal = string(str); return *this; }
#endif
    basic_value& operator=(json::string&& str) noexcept(std::is_nothrow_assignable<internal_type, json::string&&>::value) { internal = std::move(str); return *this; }

  public:
    [[nodiscard]] std::type_info const& type() const noexcept {
      return std::visit([] (auto x) -> std::type_info const& {
        return typeid(decltype(x));
      }, internal);
    }

  public:
    template<typename R = void, typename Visitor>
    constexpr R visit(Visitor&& visitor) { return std::visit(visitor, internal); }
    template<typename R = void, typename Visitor>
    constexpr R visit(Visitor&& visitor) const { return std::visit(visitor, internal); }

  public:
    template<typename T>
    [[nodiscard]] constexpr bool is() const noexcept { return std::holds_alternative<T>(internal); }
    [[nodiscard]] constexpr bool is_null() const noexcept { return is<json::null_t>(); }
    [[nodiscard]] constexpr bool is_array() const noexcept { return is<json::basic_array<Allocator>>(); }
    [[nodiscard]] constexpr bool is_boolean() const noexcept { return is<json::boolean>(); }
    [[nodiscard]] constexpr bool is_number() const noexcept { return is<json::number>(); }
      [[nodiscard]] constexpr bool is_integer() const noexcept { return is<json::number>() and number().is_integer(); }
      [[nodiscard]] constexpr bool is_decimal() const noexcept { return is<json::number>() and number().is_decimal(); }
      [[nodiscard]] constexpr bool is_u64() const noexcept { return is<json::number>() and number().template is<u64>(); }
      [[nodiscard]] constexpr bool is_i64() const noexcept { return is<json::number>() and number().template is<i64>(); }
      [[nodiscard]] constexpr bool is_f64() const noexcept { return is<json::number>() and number().template is<f64>(); }
    [[nodiscard]] constexpr bool is_object() const noexcept { return is<json::basic_object<Allocator>>(); }
    [[nodiscard]] constexpr bool is_string() const noexcept { return is<json::string>(); }

  public:
    [[nodiscard]] array_reference array() { return std::get<json::basic_array<Allocator>>(internal); }
    [[nodiscard]] array_const_reference array() const { return std::get<json::basic_array<Allocator>>(internal); }
    [[nodiscard]] boolean_reference boolean() { return std::get<json::boolean>(internal); }
    [[nodiscard]] boolean_const_reference boolean() const { return std::get<json::boolean>(internal); }
    [[nodiscard]] number_reference number() { return std::get<json::number>(internal); }
    [[nodiscard]] number_const_reference number() const { return std::get<json::number>(internal); }
    [[nodiscard]] object_reference object() { return std::get<json::basic_object<Allocator>>(internal); }
    [[nodiscard]] object_const_reference object() const { return std::get<json::basic_object<Allocator>>(internal); }
    [[nodiscard]] string_reference string() { return std::get<json::string>(internal); }
    [[nodiscard]] string_const_refernce string() const { return std::get<json::string>(internal); }

  public:
    [[nodiscard]] basic_value clone() const { return *this; }

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    [[nodiscard]] std::string dump() const { return visit<std::string>([](auto const& value) { return std::string(value.dump()); }); }
#else
    [[nodiscard]] std::u8string dump() const { return visit<std::u8string>([](auto const& value) { return value.dump(); }); }
#endif

  public:
    [[nodiscard]] bool operator==(basic_value const& rhs) const noexcept { return internal == rhs.internal; }
    [[nodiscard]] bool operator!=(basic_value const& rhs) const noexcept { return internal != rhs.internal; }
  };

}

#include <functional>

template<template<typename> typename Allocator>
struct std::hash<libjson::json::basic_value<Allocator>> {
public:
  [[nodiscard]] constexpr std::size_t operator()(libjson::json::basic_value<Allocator> const& value) const noexcept {
    return value.template visit<std::size_t>([](auto const& value) { return std::hash<std::decay_t<decltype(value)>>()(value); });
  }
};

#endif