#ifndef LIBJSON_DATA_GENERIC_DETAILS_ENTRY
#define LIBJSON_DATA_GENERIC_DETAILS_ENTRY

#include <utility>

namespace libjson::json {

  namespace details {
#if __cplusplus <= 202002L and defined(__cpp_concepts)
    template<typename T>
    concept comparable = requires (T value) {
      value == value;
      value != value;
    };
#endif
  }

  template<typename Key, typename Value>
  struct entry {
  public:
    Key const key;
    Value value;

  private:
    template<typename... KeyArgs, std::size_t... KeyIndexes, typename... ValueArgs, std::size_t... ValueIndexes>
    constexpr entry(std::tuple<KeyArgs...> key_args, std::index_sequence<KeyIndexes...>, std::tuple<ValueArgs...> value_args, std::index_sequence<ValueIndexes...>)
      : key(std::forward<KeyArgs>(std::get<KeyIndexes>(key_args))...), value(std::forward<ValueArgs>(std::get<ValueArgs>(value_args))...) {}

  public:
    constexpr entry(Key const& key, Value const& value) noexcept(std::is_nothrow_copy_constructible<Key>::value and std::is_nothrow_copy_assignable<Value>::value) : key(key), value(value) {}
    constexpr entry(Key&& key, Value&& value) noexcept(std::is_nothrow_move_constructible<Key>::value and std::is_nothrow_move_assignable<Value>::value) : key(std::forward<Key>(key)), value(std::forward<Value>(value)) {}
    template<typename... KeyArgs, typename... ValueArgs>
    constexpr entry(std::piecewise_construct_t, std::tuple<KeyArgs...> key_args, std::tuple<ValueArgs...> value_args)
      : entry(key_args, std::make_index_sequence<sizeof...(KeyArgs)>(), value_args, std::make_index_sequence<sizeof...(ValueArgs)>()) {}

  public:
    constexpr entry(entry const& rhs) : key(rhs.key.clone()), value(rhs.value.clone()) {}
    constexpr entry(entry&& rhs) noexcept : key(rhs.key.clone()), value(std::move(rhs.value)) {}

    constexpr entry& operator=(entry const& rhs) {
      key = rhs.key.clone();
      value = rhs.value.clone();
      return *this;
    }
    constexpr entry& operator=(entry&& rhs) noexcept {
      key = std::move(rhs.key);
      value = std::move(rhs.value);
      return *this;
    }
  };

}

template<typename Key, typename Value>
#if __cplusplus <= 202002L and defined(__cpp_concepts)
requires(libjson::json::details::comparable<Key> and libjson::json::details::comparable<Value>)
#endif
[[nodiscard]] constexpr bool operator==(libjson::json::entry<Key, Value> const& lhs, libjson::json::entry<Key, Value> const& rhs) noexcept(noexcept(lhs.key == rhs.key and lhs.value == rhs.value)) { return lhs.key == rhs.key and lhs.value == rhs.value; }

template<typename Key, typename Value>
#if __cplusplus <= 202002L and defined(__cpp_concepts)
requires(libjson::json::details::comparable<Key> and libjson::json::details::comparable<Value>)
#endif
[[nodiscard]] constexpr bool operator!=(libjson::json::entry<Key, Value> const& lhs, libjson::json::entry<Key, Value> const& rhs) noexcept(noexcept(lhs.key != rhs.key and lhs.value != rhs.value)) { return lhs.key != rhs.key and lhs.value != rhs.value; }


#include <functional>

template<typename Key, typename Value>
struct std::hash<libjson::json::entry<Key, Value>> {
public:
  [[nodiscard]] constexpr std::size_t operator()(libjson::json::entry<Key, Value> const& entry) noexcept {
    return std::hash<Key>()(entry.key) ^ std::hash<Value>()(entry.value);
  }
};

#endif
