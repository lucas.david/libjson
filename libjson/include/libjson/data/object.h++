#ifndef LIBJSON_DATA_OBJECT
#define LIBJSON_DATA_OBJECT

#include <libjson/data/generic/basic_object.h++>

namespace libjson::json {

  using object = basic_object<>;

}

/** Static Assertions. **/

static_assert(std::is_default_constructible<libjson::json::object>::value, "oops! libjson::json::object is not default constructible.");
static_assert(std::is_copy_constructible<libjson::json::object>::value, "oops! libjson::json::object is copy constructible.");
static_assert(std::is_move_constructible<libjson::json::object>::value, "oops! libjson::json::object is not move constructible.");
static_assert(std::is_copy_assignable<libjson::json::object>::value, "oops! libjson::json::object is copy assignable.");
static_assert(std::is_move_assignable<libjson::json::object>::value, "oops! libjson::json::object is not move assignable.");

#endif
