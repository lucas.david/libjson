#ifndef LIBJSON_DATA_NUMBER
#define LIBJSON_DATA_NUMBER

#include <string>
#include <variant>

namespace libjson::json {

  using u8  = std::uint8_t;
  using u16 = std::uint16_t;
  using u32 = std::uint32_t;
  using u64 = std::uint64_t;
  using i8  = std::int8_t;
  using i16 = std::int16_t;
  using i32 = std::int32_t;
  using i64 = std::int64_t;
  using f32 = float;
  using f64 = double;

  namespace details {

    template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
    template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

#if !(__cplusplus < 201907L)
    template<typename T> requires (std::is_integral<T>::value and !std::is_signed<T>::value)
    [[nodiscard]] constexpr u64 upscale(T value) noexcept { return static_cast<u64>(value); }

    template<typename T> requires (std::is_integral<T>::value and std::is_signed<T>::value)
    [[nodiscard]] constexpr i64 upscale(T value) noexcept { return static_cast<i64>(value); }

    template<typename T> requires (std::is_floating_point<T>::value)
    [[nodiscard]] constexpr f64 upscale(T value) noexcept { return static_cast<f64>(value); }
#endif
  }

  struct number final {
  private:
    std::variant<json::u64, json::i64, json::f64> internal;

  public:
    constexpr number() noexcept : internal(static_cast<json::f64>(0.)) {}
    ~number() noexcept = default;

#if __cplusplus < 201907L
    constexpr number(json::u8 value) noexcept : internal(static_cast<json::u64>(value)) {}  /* NOLINT: implicit conversion is intended. */
    constexpr number(json::u16 value) noexcept : internal(static_cast<json::u64>(value)) {} /* NOLINT: implicit conversion is intended. */
    constexpr number(json::u32 value) noexcept : internal(static_cast<json::u64>(value)) {} /* NOLINT: implicit conversion is intended. */
    constexpr number(json::u64 value) noexcept : internal(static_cast<json::u64>(value)) {} /* NOLINT: implicit conversion is intended. */

    constexpr number(json::i8 value) noexcept : internal(static_cast<json::i64>(value)) {}  /* NOLINT: implicit conversion is intended. */
    constexpr number(json::i16 value) noexcept : internal(static_cast<json::i64>(value)) {} /* NOLINT: implicit conversion is intended. */
    constexpr number(json::i32 value) noexcept : internal(static_cast<json::i64>(value)) {} /* NOLINT: implicit conversion is intended. */
    constexpr number(json::i64 value) noexcept : internal(static_cast<json::i64>(value)) {} /* NOLINT: implicit conversion is intended. */

    constexpr number(json::f32 value) noexcept : internal(static_cast<json::f64>(value)) {} /* NOLINT: implicit conversion is intended. */
    constexpr number(json::f64 value) noexcept : internal(static_cast<json::f64>(value)) {} /* NOLINT: implicit conversion is intended. */

    number& operator=(json::u8 value) noexcept { internal = static_cast<json::u64>(value); return *this; }  /* NOLINT: implicit conversion is intended. */
    number& operator=(json::u16 value) noexcept { internal = static_cast<json::u64>(value); return *this; } /* NOLINT: implicit conversion is intended. */
    number& operator=(json::u32 value) noexcept { internal = static_cast<json::u64>(value); return *this; } /* NOLINT: implicit conversion is intended. */
    number& operator=(json::u64 value) noexcept { internal = static_cast<json::u64>(value); return *this; } /* NOLINT: implicit conversion is intended. */

    number& operator=(json::i8 value) noexcept { internal = static_cast<json::u64>(value); return *this; }  /* NOLINT: implicit conversion is intended. */
    number& operator=(json::i16 value) noexcept { internal = static_cast<json::u64>(value); return *this; } /* NOLINT: implicit conversion is intended. */
    number& operator=(json::i32 value) noexcept { internal = static_cast<json::u64>(value); return *this; } /* NOLINT: implicit conversion is intended. */
    number& operator=(json::i64 value) noexcept { internal = static_cast<json::u64>(value); return *this; } /* NOLINT: implicit conversion is intended. */

    number& operator=(json::f32 value) noexcept { internal = static_cast<json::u64>(value); return *this; } /* NOLINT: implicit conversion is intended. */
    number& operator=(json::f64 value) noexcept { internal = static_cast<json::u64>(value); return *this; } /* NOLINT: implicit conversion is intended. */
#else
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    constexpr number(T value) noexcept : internal(details::upscale(value)) {} /* NOLINT */

    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    constexpr number& operator=(T value) noexcept { internal = details::upscale(value); return *this; }
#endif

  public:
    template<typename R = void, typename Visitor>
    constexpr R visit(Visitor&& visitor) { return std::visit(visitor, internal); }
    template<typename R = void, typename Visitor>
    constexpr R visit(Visitor&& visitor) const { return std::visit(visitor, internal); }

  public:
    template<typename T>
    [[nodiscard]] constexpr bool is() const noexcept { return std::holds_alternative<T>(internal); }
    [[nodiscard]] constexpr bool is_integer() const noexcept { return is<json::u64>() or is<json::i64>(); }
    [[nodiscard]] constexpr bool is_decimal() const noexcept { return is<json::f64>(); }
    [[nodiscard]] constexpr bool is_u64() const noexcept { return is<json::u64>(); }
    [[nodiscard]] constexpr bool is_i64() const noexcept { return is<json::i64>(); }
    [[nodiscard]] constexpr bool is_f64() const noexcept { return is<json::f64>(); }

  public: /* accessor. */
    [[nodiscard]] constexpr json::u64& u64() { return std::get<json::u64>(internal); }
    [[nodiscard]] constexpr json::u64 u64() const { return std::get<json::u64>(internal); }
    [[nodiscard]] constexpr json::i64& i64() { return std::get<json::i64>(internal); }
    [[nodiscard]] constexpr json::i64 i64() const { return std::get<json::i64>(internal); }
    [[nodiscard]] constexpr json::f64& f64() { return std::get<json::f64>(internal); }
    [[nodiscard]] constexpr json::f64 f64() const { return std::get<json::f64>(internal); }

  public: /* forced conversion. */
    [[nodiscard]] constexpr json::u64 as_u64() const { return visit<json::u64>([](auto value) { return static_cast<json::u64>(value); }); }
    [[nodiscard]] constexpr json::i64 as_i64() const { return visit<json::i64>([](auto value) { return static_cast<json::i64>(value); }); }
    [[nodiscard]] constexpr json::f64 as_f64() const { return visit<json::f64>([](auto value) { return static_cast<json::f64>(value); }); }

  public:
    [[nodiscard]] number clone() const { return *this; }

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    [[nodiscard]] std::string dump() const { return visit<std::string>([](auto value) { return std::to_string(value); }); }
#else
    [[nodiscard]] std::u8string dump() const { return visit<std::u8string>([](auto value) { return std::to_string(value); }); }
#endif

  public:
    [[nodiscard]] constexpr bool operator==(number const& rhs) const noexcept { return rhs.visit<bool>([&] (auto value) { return operator==(value); }); }
    [[nodiscard]] constexpr bool operator!=(number const& rhs) const noexcept { return rhs.visit<bool>([&] (auto value) { return operator!=(value); }); }
    [[nodiscard]] constexpr bool operator<(number const& rhs) const noexcept { return rhs.visit<bool>([&] (auto value) { return operator<(value); }); }
    [[nodiscard]] constexpr bool operator<=(number const& rhs) const noexcept { return rhs.visit<bool>([&] (auto value) { return operator<=(value); }); }
    [[nodiscard]] constexpr bool operator>(number const& rhs) const noexcept { return rhs.visit<bool>([&] (auto value) { return operator>(value); }); }
    [[nodiscard]] constexpr bool operator>=(number const& rhs) const noexcept { return rhs.visit<bool>([&] (auto value) { return operator>=(value); }); }

#if __cplusplus < 201907L
    [[nodiscard]] constexpr bool operator==(json::u8 rhs) const noexcept { return operator==(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator!=(json::u8 rhs) const noexcept { return operator!=(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator<(json::u8 rhs) const noexcept { return operator<(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator<=(json::u8 rhs) const noexcept { return operator<=(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator>(json::u8 rhs) const noexcept { return operator>(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator>=(json::u8 rhs) const noexcept { return operator>=(static_cast<json::u64>(rhs)); }

    [[nodiscard]] constexpr bool operator==(json::u16 rhs) const noexcept { return operator==(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator!=(json::u16 rhs) const noexcept { return operator!=(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator<(json::u16 rhs) const noexcept { return operator<(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator<=(json::u16 rhs) const noexcept { return operator<=(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator>(json::u16 rhs) const noexcept { return operator>(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator>=(json::u16 rhs) const noexcept { return operator>=(static_cast<json::u64>(rhs)); }

    [[nodiscard]] constexpr bool operator==(json::u32 rhs) const noexcept { return operator==(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator!=(json::u32 rhs) const noexcept { return operator!=(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator<(json::u32 rhs) const noexcept { return operator<(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator<=(json::u32 rhs) const noexcept { return operator<=(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator>(json::u32 rhs) const noexcept { return operator>(static_cast<json::u64>(rhs)); }
    [[nodiscard]] constexpr bool operator>=(json::u32 rhs) const noexcept { return operator>=(static_cast<json::u64>(rhs)); }

    [[nodiscard]] constexpr bool operator==(json::u64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::u64 value) { return value == rhs; },
        [&](auto value) { return value == static_cast<decltype(value)>(rhs) and static_cast<json::u64>(value) == rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator!=(json::u64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::u64 value) { return value != rhs; },
        [&](auto value) { return value != static_cast<decltype(value)>(rhs) and static_cast<json::u64>(value) != rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator<(json::u64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::u64 value) { return value < rhs; },
        [&](auto value) { return value < static_cast<decltype(value)>(rhs) and static_cast<json::u64>(value) < rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator<=(json::u64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::u64 value) { return value <= rhs; },
        [&](auto value) { return value <= static_cast<decltype(value)>(rhs) and static_cast<json::u64>(value) <= rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator>(json::u64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::u64 value) { return value > rhs; },
        [&](auto value) { return value > static_cast<decltype(value)>(rhs) and static_cast<json::u64>(value) > rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator>=(json::u64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::u64 value) { return value >= rhs; },
        [&](auto value) { return value >= static_cast<decltype(value)>(rhs) and static_cast<json::u64>(value) >= rhs; }
      });
    }

    [[nodiscard]] constexpr bool operator==(json::i8 rhs) const noexcept { return operator==(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator!=(json::i8 rhs) const noexcept { return operator!=(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator<(json::i8 rhs) const noexcept { return operator<(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator<=(json::i8 rhs) const noexcept { return operator<=(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator>(json::i8 rhs) const noexcept { return operator>(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator>=(json::i8 rhs) const noexcept { return operator>=(static_cast<json::i64>(rhs)); }

    [[nodiscard]] constexpr bool operator==(json::i16 rhs) const noexcept { return operator==(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator!=(json::i16 rhs) const noexcept { return operator!=(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator<(json::i16 rhs) const noexcept { return operator<(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator<=(json::i16 rhs) const noexcept { return operator<=(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator>(json::i16 rhs) const noexcept { return operator>(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator>=(json::i16 rhs) const noexcept { return operator>=(static_cast<json::i64>(rhs)); }

    [[nodiscard]] constexpr bool operator==(json::i32 rhs) const noexcept { return operator==(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator!=(json::i32 rhs) const noexcept { return operator!=(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator<(json::i32 rhs) const noexcept { return operator<(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator<=(json::i32 rhs) const noexcept { return operator<=(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator>(json::i32 rhs) const noexcept { return operator>(static_cast<json::i64>(rhs)); }
    [[nodiscard]] constexpr bool operator>=(json::i32 rhs) const noexcept { return operator>=(static_cast<json::i64>(rhs)); }

    [[nodiscard]] constexpr bool operator==(json::i64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::i64 value) { return value == rhs; },
        [&](auto value) { return value == static_cast<decltype(value)>(rhs) and static_cast<json::i64>(value) == rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator!=(json::i64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::i64 value) { return value != rhs; },
        [&](auto value) { return value != static_cast<decltype(value)>(rhs) and static_cast<json::i64>(value) != rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator<(json::i64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::i64 value) { return value < rhs; },
        [&](auto value) { return value < static_cast<decltype(value)>(rhs) and static_cast<json::i64>(value) < rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator<=(json::i64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::i64 value) { return value <= rhs; },
        [&](auto value) { return value <= static_cast<decltype(value)>(rhs) and static_cast<json::i64>(value) <= rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator>(json::i64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::i64 value) { return value > rhs; },
        [&](auto value) { return value > static_cast<decltype(value)>(rhs) and static_cast<json::i64>(value) > rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator>=(json::i64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::i64 value) { return value >= rhs; },
        [&](auto value) { return value >= static_cast<decltype(value)>(rhs) and static_cast<json::i64>(value) >= rhs; }
      });
    }

    [[nodiscard]] constexpr bool operator==(json::f32 rhs) const noexcept { return operator==(static_cast<json::f64>(rhs)); }
    [[nodiscard]] constexpr bool operator!=(json::f32 rhs) const noexcept { return operator!=(static_cast<json::f64>(rhs)); }
    [[nodiscard]] constexpr bool operator<(json::f32 rhs) const noexcept { return operator<(static_cast<json::f64>(rhs)); }
    [[nodiscard]] constexpr bool operator<=(json::f32 rhs) const noexcept { return operator<=(static_cast<json::f64>(rhs)); }
    [[nodiscard]] constexpr bool operator>(json::f32 rhs) const noexcept { return operator>(static_cast<json::f64>(rhs)); }
    [[nodiscard]] constexpr bool operator>=(json::f32 rhs) const noexcept { return operator>=(static_cast<json::f64>(rhs)); }

    [[nodiscard]] constexpr bool operator==(json::f64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::f64 value) { return value == rhs; },
        [&](auto value) { return value == static_cast<decltype(value)>(rhs) and static_cast<json::f64>(value) == rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator!=(json::f64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::f64 value) { return value != rhs; },
        [&](auto value) { return value != static_cast<decltype(value)>(rhs) and static_cast<json::f64>(value) != rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator<(json::f64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::f64 value) { return value < rhs; },
        [&](auto value) { return value < static_cast<decltype(value)>(rhs) and static_cast<json::f64>(value) < rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator<=(json::f64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::f64 value) { return value <= rhs; },
        [&](auto value) { return value <= static_cast<decltype(value)>(rhs) and static_cast<json::f64>(value) <= rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator>(json::f64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::f64 value) { return value > rhs; },
        [&](auto value) { return value > static_cast<decltype(value)>(rhs) and static_cast<json::f64>(value) > rhs; }
      });
    }
    [[nodiscard]] constexpr bool operator>=(json::f64 rhs) const noexcept {
      return visit<bool>(details::overloaded {
        [&](json::f64 value) { return value >= rhs; },
        [&](auto value) { return value >= static_cast<decltype(value)>(rhs) and static_cast<json::f64>(value) >= rhs; }
      });
    }
#else
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    [[nodiscard]] constexpr bool operator==(T rhs) const noexcept {
      return visit<bool>({
        [&](T value) { return value == rhs; },
        [&](auto value) { return value == static_cast<decltype(value)>(rhs) and static_cast<T>(value) == rhs; }
      });
    }
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    [[nodiscard]] constexpr bool operator!=(T rhs) const noexcept {
      return visit<bool>({
        [&](T value) { return value != rhs; },
        [&](auto value) { return value != static_cast<decltype(value)>(rhs) and static_cast<T>(value) != rhs; }
      });
    }
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    [[nodiscard]] constexpr bool operator<(T rhs) const noexcept {
      return visit<bool>({
        [&](T value) { return value < rhs; },
        [&](auto value) { return value < static_cast<decltype(value)>(rhs) and static_cast<T>(value) < rhs; }
      });
    }
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    [[nodiscard]] constexpr bool operator<=(T rhs) const noexcept {
      return visit<bool>({
        [&](T value) { return value <= rhs; },
        [&](auto value) { return value <= static_cast<decltype(value)>(rhs) and static_cast<T>(value) <= rhs; }
      });
    }
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    [[nodiscard]] constexpr bool operator>(T rhs) const noexcept {
      return visit<bool>({
        [&](T value) { return value > rhs; },
        [&](auto value) { return value > static_cast<decltype(value)>(rhs) and static_cast<T>(value) > rhs; }
      });
    }
    template<typename T> requires (std::is_arithmetic<T>::value and !std::is_same<T, bool>::value)
    [[nodiscard]] constexpr bool operator>=(T rhs) const noexcept {
      return visit<bool>({
        [&](T value) { return value >= rhs; },
        [&](auto value) { return value >= static_cast<decltype(value)>(rhs) and static_cast<T>(value) >= rhs; }
      });
    }
#endif
  };

}

#include <functional>

template<> struct std::hash<libjson::json::number> {
public:
  constexpr std::size_t operator()(libjson::json::number const& number) const noexcept {
    return number.visit<std::size_t>([](auto value) { return std::hash<decltype(value)>()(value); });
  }
};

/** Static Assertions. **/

static_assert(std::is_default_constructible<libjson::json::number>::value, "oops! libjson::json::number is not default constructible.");
static_assert(std::is_copy_constructible<libjson::json::number>::value, "oops! libjson::json::number is not copy constructible.");
static_assert(std::is_move_constructible<libjson::json::number>::value, "oops! libjson::json::number is not move constructible.");
static_assert(std::is_copy_assignable<libjson::json::number>::value, "oops! libjson::json::number is not copy assignable.");
static_assert(std::is_move_assignable<libjson::json::number>::value, "oops! libjson::json::number is not move assignable.");

#endif
