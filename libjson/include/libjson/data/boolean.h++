#ifndef LIBJSON_DATA_BOOLEAN
#define LIBJSON_DATA_BOOLEAN

namespace libjson::json {

  struct boolean  {
  private:
    bool internal = false;

  public:
    constexpr boolean() noexcept : internal(false) {}
    constexpr boolean(bool value) noexcept : internal(value) {} /* NOLINT: implicit conversion is intended. */

  public:
    constexpr operator bool() const noexcept { return internal; } /* NOLINT: implicit conversion is intended. */

  public:
    [[nodiscard]] constexpr boolean clone() const { return *this; }

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    [[nodiscard]] constexpr char const* dump() const { return internal ? "true" : "false"; }
#else
    [[nodiscard]] constexpr char8_t const* dump() const { return internal ? u8"true" : u8"false"; }
#endif

  public:
    [[nodiscard]] constexpr bool operator==(bool rhs) const noexcept { return internal == rhs; }
    [[nodiscard]] constexpr bool operator!=(bool rhs) const noexcept { return internal != rhs; }
    [[nodiscard]] constexpr bool operator<(bool rhs) const noexcept { return internal < rhs; }
    [[nodiscard]] constexpr bool operator<=(bool rhs) const noexcept { return internal <= rhs; }
    [[nodiscard]] constexpr bool operator>(bool rhs) const noexcept { return internal > rhs; }
    [[nodiscard]] constexpr bool operator>=(bool rhs) const noexcept { return internal >= rhs; }
    [[nodiscard]] constexpr bool operator==(boolean rhs) const noexcept { return internal == rhs.internal; }
    [[nodiscard]] constexpr bool operator!=(boolean rhs) const noexcept { return internal != rhs.internal; }
    [[nodiscard]] constexpr bool operator<(boolean rhs) const noexcept { return internal < rhs.internal; }
    [[nodiscard]] constexpr bool operator<=(boolean rhs) const noexcept { return internal <= rhs.internal; }
    [[nodiscard]] constexpr bool operator>(boolean rhs) const noexcept { return internal > rhs.internal; }
    [[nodiscard]] constexpr bool operator>=(boolean rhs) const noexcept { return internal >= rhs.internal; }
  };

}

/** Static Assertions. */
static_assert(std::is_default_constructible<libjson::json::boolean>::value, "oops! libjson::json::boolean is not default constructible.");
static_assert(std::is_copy_constructible<libjson::json::boolean>::value, "oops! libjson::json::boolean is not copy constructible.");
static_assert(std::is_move_constructible<libjson::json::boolean>::value, "oops! libjson::json::boolean is not move constructible.");
static_assert(std::is_copy_assignable<libjson::json::boolean>::value, "oops! libjson::json::boolean is not copy assignable.");
static_assert(std::is_move_assignable<libjson::json::boolean>::value, "oops! libjson::json::boolean is not move assignable.");

#endif