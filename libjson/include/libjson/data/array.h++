#ifndef LIBJSON_DATA_ARRAY
#define LIBJSON_DATA_ARRAY

#include <libjson/data/generic/basic_array.h++>

namespace libjson::json {
  using array = basic_array<>;
}

template<>
constexpr void std::swap(libjson::json::array& lhs, libjson::json::array& rhs) noexcept(noexcept(lhs.swap(rhs))) { lhs.swap(rhs); }


/** Static Assertions. **/

static_assert(std::is_default_constructible<libjson::json::array>::value, "oops! libjson::json::array is not default constructible.");
static_assert(std::is_copy_constructible<libjson::json::array>::value, "oops! libjson::json::array is copy constructible.");
static_assert(std::is_move_constructible<libjson::json::array>::value, "oops! libjson::json::array is not move constructible.");
static_assert(std::is_copy_assignable<libjson::json::array>::value, "oops! libjson::json::array is copy assignable.");
static_assert(std::is_move_assignable<libjson::json::array>::value, "oops! libjson::json::array is not move assignable.");

#endif