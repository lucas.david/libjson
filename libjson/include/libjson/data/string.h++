#ifndef LIBJSON_DATA_STRING
#define LIBJSON_DATA_STRING

#include <cstring>
#include <string>

namespace libjson::json {

  namespace details {

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    constexpr char hexadecimal(char c) noexcept {
      c &= 0x0f;
      if (c < 0x0a)
        return static_cast<char>('0' + c);
      return static_cast<char>('a' + c - 10);
    }
#else
    constexpr char8_t hexadecimal(char8_t c) noexcept {
      c &= 0x0f;
      if (c < 0x0a)
        return static_cast<char8_t>('0' + c);
      return static_cast<char8_t>('a' + c - 10);
    }
#endif

  }

#if 202002 <= __cplusplus
  struct string : public std::u8string {
#else
  struct string final : public std::string {
#endif
  public:
    string(string const&) = default;
    string& operator=(string const&) = default;

  public:
    string() = default;
    string(string&&) = default;

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    string(char const* str) : std::string(str) {}                  /* NOLINT: implicit conversion is intended. */
#else
    constexpr string_t(char8_t const* str) : std::u8string(str) {} /* NOLINT: implicit conversion is intended. */
#endif

    string& operator=(string&&) noexcept = default;

  private:
#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    static std::string escape(string const& str) {
      std::string result; result.reserve(str.size());
      for (auto c : str) {
        switch (c) {
        case '\b': result += "\\b"; break;
        case '\t': result += "\\t"; break;
        case '\n': result += "\\n"; break;
        case '\f': result += "\\f"; break;
        case '\r': result += "\\r"; break;
        case '"':  result += "\\\""; break;
        case '\\': result += "\\\\"; break;
        default:
          if (c < 0x20) {
            result += "\\u00";
            result += details::hexadecimal(static_cast<char>(c / 0x0f));
            result += details::hexadecimal(static_cast<char>(c % 0x0f));
          } else
            result += c;
        }
      }
      return result;
    }
#else
    static std::u8string escape(string const& str) {
      std::u8string result; result.reserve(str.size());
      for (auto c : str) {
        switch (c) {
        case u8'\b': result += u8"\\b"; break;
        case u8'\t': result += u8"\\t"; break;
        case u8'\n': result += u8"\\n"; break;
        case u8'\f': result += u8"\\f"; break;
        case u8'\r': result += u8"\\r"; break;
        case u8'"':  result += u8"\\\""; break;
        case u8'\\': result += u8"\\\\"; break;
        default:
          if (c < 0x20) {
            result += u8"\\u00";
            result += details::hexadecimal(static_cast<char>(c / 0x0f));
            result += details::hexadecimal(static_cast<char>(c % 0x0f));
          } else
            result += c;
        }
      }
      return result;
    }
#endif

  public:
#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    string& replace(char old, char replacement) noexcept {
      for (auto& c : *this)
        if (c == old)
          c = replacement;
      return *this;
    }
#else
    string& replace(char8_t const* sequence, char8_t const* replacement) {
      for (auto position = find(sequence); position != std::string::npos; position = find(sequence))
        std::basic_string<char8_t>::replace(position, std::strlen(sequence), replacement, std::strlen(replacement));
      return *this;
    }
#endif

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    string& replace(char const* sequence, char const* replacement) {
      for (auto position = find(sequence); position != std::string::npos; position = find(sequence))
        std::basic_string<char>::replace(position, std::strlen(sequence), replacement, std::strlen(replacement));
      return *this;
    }
#else
    string& replace(char8_t const* sequence, char8_t const* replacement) {
      for (auto position = find(sequence); position != std::string::npos; position = find(sequence))
        std::basic_string<char8_t>::replace(position, std::strlen(sequence), replacement, std::strlen(replacement));
      return *this;
    }
#endif

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    string& replace(std::string const& sequence, std::string const& replacement) {
      for (auto position = find(sequence); position != std::string::npos; position = find(sequence))
        std::basic_string<char>::replace(position, sequence.length(), replacement, replacement.length());
      return *this;
    }
#else
    string& replace(std::u8string const& sequence, std::u8string const& replacement) {
      for (auto position = find(sequence); position != std::string::npos; position = find(sequence))
        std::basic_string<char8_t>::replace(position, sequence.length(), replacement, replacement.length());
      return *this;
    }
#endif


#if __cplusplus < 202002
    [[nodiscard]] bool starts_with(char prefix) const { return length() != 0 and front() == prefix; }

    [[nodiscard]] bool starts_with(char const* prefix) const {
      if (prefix == nullptr)
        return false;
      for (std::size_t index = 0; prefix[index] != '\0'; ++index)
        if (index == length() or operator[](index) != prefix[index])
          return false;
      return true;
    }
#endif

  public:
    [[nodiscard]] string clone() const { return *this; }

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    [[nodiscard]] std::string dump() const { return '"' + escape(*this) + '"'; }
#else
    [[nodiscard]] std::u8string dump() const { return u8'"' + escape(*this) + u8'"'; }
#endif
  };

}

/** Static Assertions. **/

static_assert(std::is_default_constructible<libjson::json::string>::value, "oops! libjson::json::string is not default constructible.");
static_assert(std::is_copy_constructible<libjson::json::string>::value, "oops! libjson::json::string is copy constructible.");
static_assert(std::is_move_constructible<libjson::json::string>::value, "oops! libjson::json::string is not move constructible.");
static_assert(std::is_copy_assignable<libjson::json::string>::value, "oops! libjson::json::string is copy assignable.");
static_assert(std::is_move_assignable<libjson::json::string>::value, "oops! libjson::json::string is not move assignable.");

#endif