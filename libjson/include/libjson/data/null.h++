#ifndef LIBJSON_DATA_NULL
#define LIBJSON_DATA_NULL

namespace libjson::json {

  struct null_t {
  public:
    [[nodiscard]] constexpr null_t clone() const { return *this; }

#if __cplusplus < 201907L /* C++20 is 202002L and UTF-8 char core and library features are 201907L. */
    [[nodiscard]] constexpr char const* dump() const { return "null"; }
#else
    [[nodiscard]] constexpr char8_t const* dump() const { return u8"null"; }
#endif

  public:
    [[nodiscard]] constexpr bool operator==(null_t) const noexcept { return true; }
    [[nodiscard]] constexpr bool operator!=(null_t) const noexcept { return false; }
  };



  constexpr null_t null = {};

}

#include <functional>

template<> struct std::hash<libjson::json::null_t> {
public:
  constexpr std::size_t operator()(libjson::json::null_t) const noexcept { return 42; }
};

/** Static Assertions. **/

static_assert(std::is_default_constructible<libjson::json::null_t>::value, "oops! libjson::json::null is not default constructible.");
static_assert(std::is_copy_constructible<libjson::json::null_t>::value, "oops! libjson::json::null is not copy constructible.");
static_assert(std::is_move_constructible<libjson::json::null_t>::value, "oops! libjson::json::null is not move constructible.");
static_assert(std::is_copy_assignable<libjson::json::null_t>::value, "oops! libjson::json::null is not copy assignable.");
static_assert(std::is_move_assignable<libjson::json::null_t>::value, "oops! libjson::json::null is not move assignable.");

#endif