#ifndef LIBJSON_DATA_VALUE
#define LIBJSON_DATA_VALUE

#include <libjson/data/generic/basic_value.h++>

#include <libjson/data/null.h++>
#include <libjson/data/array.h++>
#include <libjson/data/boolean.h++>
#include <libjson/data/number.h++>
#include <libjson/data/object.h++>
#include <libjson/data/string.h++>

namespace libjson::json {

  using value = basic_value<>;

}

/** Static Assertions. **/

static_assert(std::is_default_constructible<libjson::json::value>::value, "oops! libjson::json::value is not default constructible.");
static_assert(!std::is_copy_constructible<libjson::json::value>::value, "oops! libjson::json::value is copy constructible.");
static_assert(std::is_move_constructible<libjson::json::value>::value, "oops! libjson::json::value is not move constructible.");
static_assert(!std::is_copy_assignable<libjson::json::value>::value, "oops! libjson::json::value is copy assignable.");
static_assert(std::is_move_assignable<libjson::json::value>::value, "oops! libjson::json::value is not move assignable.");

#endif