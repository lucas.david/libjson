#include <libjson/data/number.h++>

#include <ut>

boost::ut::suite number = [] {
  using namespace libjson;
  using namespace boost::ut;

  "json::number()"_test = [] {
    json::number x;
    expect(json::number().is_u64());
    expect(json::number() == 0u);
    expect(json::number() == 0);
    expect(json::number() == 0.);
  };

  "json::number(json::number&&)"_test = [] {
    expect(json::number(json::number(42u)) == 42u);
    expect(json::number(json::number(42)) == 42);
    expect(json::number(json::number(4.2)) == 4.2);
  };

  "json::number(json::u8)"_test  = [] { expect(json::number(static_cast<json::u8>(42u)) == 42u);  };
  "json::number(json::u16)"_test = [] { expect(json::number(static_cast<json::u16>(42u)) == 42u); };
  "json::number(json::u32)"_test = [] { expect(json::number(static_cast<json::u32>(42u)) == 42u); };
  "json::number(json::u64)"_test = [] { expect(json::number(static_cast<json::u64>(42u)) == 42u); };

  "json::number(json::i8)"_test  = [] { expect(json::number(static_cast<json::i8>(42)) == 42);  };
  "json::number(json::i16)"_test = [] { expect(json::number(static_cast<json::i16>(42)) == 42); };
  "json::number(json::i32)"_test = [] { expect(json::number(static_cast<json::i32>(42)) == 42); };
  "json::number(json::i64)"_test = [] { expect(json::number(static_cast<json::i64>(42)) == 42); };

  "json::number(json::f32)"_test = [] { expect(json::number(static_cast<json::f32>(4.2)) == 4.2); };
  "json::number(json::f64)"_test = [] { expect(json::number(static_cast<json::f64>(4.2)) == 4.2); };

};

