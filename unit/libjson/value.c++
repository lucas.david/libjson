#include <libjson/data/value.h++>

#include <ut>

using namespace libjson;

boost::ut::suite value = [] {
  using namespace boost::ut;
  using namespace std::string_literals;

  "json::value()"_test = [] { expect(json::value().is_null()); };
  "json::value(json::value&&)"_test = [&] {
    std::array<json::value, 6> values = {json::null, json::array({"libjson", true, 69}), false, 69, json::object({{"lib", "json"}}), "libjson"};
    std::array<json::value, 6> const moved = std::move(values);

    spec::describe("json::value moved are becoming json::null") = [&] { /* NOLINT: uses values after move is intended. */
      for (std::size_t index = 0; index < 6; ++index)
        expect(values[index].is_null());
    };

    spec::describe("json::value move constructed are the same as before") = [&] {
      expect(moved[0].is_null()) << "[json::null = " + moved[0].dump() + "]";
      expect(moved[1] == json::array({"libjson", true, 69})) << "[json::array = " + moved[1].dump() + "]";
      expect(moved[2] == false) << "[json::boolean = " + moved[2].dump() + "]";
      expect(moved[3] == 69) << "[json::number = " + moved[3].dump() + "]";
      expect(moved[4] == json::object({{"lib", "json"}})) << "[json::object = " + moved[4].dump() + "]";
      expect(moved[5] == "libjson") << "[json::string = " + moved[5].dump() + "]";
    };

  };

  /* json::null related constructors. */
  "json::value(null)"_test = [] {
    expect(json::value(json::null).is_null());
    expect(nothrow([] { return json::value(json::null) == json::null; }));
  };

  /* json::boolean related constructors. */
  "json::value(bool)"_test = [] {
    expect(json::value(true).is_boolean() and json::value(false).is_boolean());
    expect(json::value(true).boolean() == true);
    expect(json::value(false).boolean() == false);
  };
  "json::value(boolean_type)"_test = [] {
    expect(json::value(json::boolean(true)).is_boolean() and json::value(json::boolean(false)).is_boolean());
    expect(nothrow([] {
      return json::value(json::boolean(true)) == json::boolean(true)
        and json::value(json::boolean(false)) == json::boolean(false);
    }));
  };

  /* json::number related constructors. */
  "<T> json::value(T)"_test = [] {
    expect(json::value(69).is_integer() and json::value(69).is_i64());
    expect(json::value(69l).is_integer() and json::value(69l).is_i64());
    /* expect(json::value(69ll).is_integer() and json::value(69ll).is_i64()); */

    expect(json::value(69u).is_integer() and json::value(69u).is_u64());
    expect(json::value(69ul).is_integer() and json::value(69ul).is_u64());
    /* expect(json::value(69ull).is_integer() and json::value(69ull).is_u64()); */

    expect(json::value(69.f).is_decimal() and json::value(69.f).is_f64());
    expect(json::value(69.).is_decimal() and json::value(69.).is_f64());
    /* expect(json::value(69.l).is_decimal() and json::value(69.l).is_f64()); */

    expect(nothrow([] {
      expect(json::value(69) == 69);
      expect(json::value(69l) == 69l);
      /* expect(json::value(69ll) == 69ll); */
      expect(json::value(69u) == 69u);
      expect(json::value(69ul) == 69ul);
      /* expect(json::value(69ull) == 69ull); */
      expect(json::value(69.f) == 69.);
      expect(json::value(69.f) == 69.);
    }));
  };
  "json::value(number&&)"_test = [] {
    expect(json::value(json::number(69)) == json::number(69)) << "[json::value moved = " + json::number(69).dump() + "]";
  };




};


int main() { return 0; }